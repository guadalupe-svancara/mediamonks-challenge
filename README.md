## MediaMonks Challenge

¡Hola MediaMonks! A continuación dejo la descripción de mi resolución para este Challenge.

**Repositorio**

Tal como fue solicitado pueden acceder al repositorio siguiendo este link:

https://bitbucket.org/guadalupe-svancara/mediamonks-challenge/src/master/

---

**RESTful API**

La API provee endpoints para crear, modificar, obtener y eliminar pares de valores. 

El servidor está desarrollado según la arquitectura REST con la ayuda de las siguientes herramientas:

1. Express para resolver las peticiones HTTP

2. Mongoose para realizar las operaciones de bases de datos

3. body-parser para acceder al body de las peticiones en formato JSON

---

**Instrucciones para poner el servidor en funcionamiento**

1. Utilizar un CLI y posicionarse en su dirección de preferencia en el sistema de archivos

2. Clonar el repositorio Git

**git clone https://guadalupe-svancara@bitbucket.org/guadalupe-svancara/mediamonks-challenge.git**

Esta operación clonará el contenido del repositorio en una nueva carpeta **"mediamonks-challenge"**

3. Ejecutar el comando **npm install**

4. Ejecutar el comando **node server**

El puerto configurado es 3000.

---

**Modelo de datos**

Para implementar las operaciones realicé un pequeño modelo Mongo **KeyValue** con dos propiedades:

1. key: numérico

2. value: String

---

**Endpoints**

**/newPair**

Método POST para almacenar pares de datos key, value. 

- Dada una clave y un valor se crea una instancia de KeyValue y se almacena el documento correspondiente en la base de datos.

- En caso de no proveer la clave se retorna una respuesta **HTTP 400 - bad request**

- En caso de existir la clave en algún par se retorna una respuesta **HTTP 400 - bad request**

- En caso de no proveer un valor se crea la instancia del par asignando como valor la cadena **'default'**

**/getValue**

Método GET para obtener un value. Dada una clave se busca el documento correspondiente en la base de datos.

- En caso de no proveer la clave se retorna una respuesta **HTTP 400 - bad request**

- En caso de no existir la clave en ningún par se retorna una respuesta **HTTP 404 - not found**

**/getAllPairs**

Método GET para obtener todos los pares de datos key, value almacenados.

**/updateValue**

Método PUT para modificar un value. Dada una clave se busca el documento correspondiente en la base de datos y se actualiza la propiedad value con el nuevo valor deseado.

- En caso de no proveer la clave se retorna una respuesta **HTTP 400 - bad request**

- En caso de no proveer el valor se retorna una respuesta **HTTP 400 - bad request**

- En caso de no existir la clave en ningún par se retorna una respuesta **HTTP 404 - not found**

**/deletePair**

Método DELETE para eliminar pares de datos key, value. Dada una clave se busca el documento correspondiente en la base de datos y se elimina.

- En caso de no proveer la clave se retorna una respuesta **HTTP 400 - bad request**

- En caso de no existir la clave en ningún par se retorna una respuesta **HTTP 404 - not found**

---

**Respuestas HTTP 200 - OK**

Utilizadas para los casos en que se resuelvan las peticiones de manera exitosa con los siguientes datos para los respectivos endpoints, junto con un mensaje acorde:

- /newPair: key, value

- /getValue: value

- /updateValue: key, value actualizado

- /deletePair: key

---

**Respuestas de error HTTP 500 - Internal Server Error**

Utilizadas para los casos en que se produzcan errores en las operaciones de base de datos.

---

**Pruebas**

Para realizar pruebas sobre los endpoints utilicé la herramienta Postman, indicando 

1. el tipo de petición correspondiente (POST/GET/PUT/DELETE)

2. dirección local en la que corre el servidor http://localhost:3000/ sumado al endpoint deseado

3. datos a enviar en el body para los respectivos endpoints:

- /newPair: key numérico, value texto (en caso de no recibir value se coloca 'default')

- /getValue: key numérico

- /updateValue: key numérico, value texto

- /deletePair: key numérico

El formato de codificación de los datos enviados en el body es esapplication/x-www-form-urlencoded

---

**Conexiones websocket**

Intenté implementar esta parte del Challenge con la ayuda del paquete https://www.npmjs.com/package/websocket. Aunque logré instanciar un servidor y ponerlo a la escucha no pude resolver el requisito de admitir múltiples sockets ya que en mi trabajo actual no lo he puesto en práctica.

**Aprovecho esta oportunidad para agradecerles por su tiempo y por haberme considerado para participar del proceso de selección.**

Guadalupe Svancara