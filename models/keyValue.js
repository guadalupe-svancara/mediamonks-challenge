var mongoose = require('mongoose'),
Schema = mongoose.Schema,
KeyValueSchema = new Schema({
    key: Number,
    value: String
});

KeyValueSchema.static('findByKey', function(wantedKey, callback) {
    return this.find({key: wantedKey}, callback);
});

KeyValueSchema.static( 'findByValue', function(wantedValue, callback) {
    return this.find({value: wantedValue}, callback);
});

module.exports = mongoose.model('KeyValue', KeyValueSchema);