const express = require("express");
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const KeyValue = require('./models/keyValue');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/keyValueDB');

/* create and save a (key, value) pair */
app.post('/newPair', function (req, res) {
    let newKey = req.body.key;
    let newValue = req.body.value;

    if (!newKey) {
        /* bad request: no key sent */
        res.status(400).send({error: 'no key specified'});
    } else {
        /* check whether the key sent already exists */
        KeyValue.findByKey(newKey, function(error, pairs) {
            if (error) {
                res.status(500).send({error: 'error trying to save pair: '+error, key: newKey});
            } else if (pairs.length === 0) {
                newPair = new KeyValue();
                newPair.key = newKey;
                newPair.value = newValue ? newValue : 'default';
                newPair.save(function (error) {
                    if (error) {
                        res.status(500).send({error: 'error trying to save pair: '+error, key: newKey});
                    } else {
                        res.status(200).send({message: 'pair saved successfully!', key: newPair.key, value: newPair.value});
                    }
                });
            } else {
                /* bad request: key already exists */
                res.status(400).send({error: 'the key already exists', key: newKey});
            }
        });
    }
});

/* given a key, retrieve it's value */
app.get('/getValue', function (req, res) {
    let searchKey = req.body.key;
    if (!searchKey) {
        /* bad request: no key sent */
        res.status(400).send({error: 'no key specified'});
    } else {
        KeyValue.findByKey(searchKey, function(error, pairs) {
            if (error) {
                res.status(500).send({error: 'error trying to fetch pair: '+error, key: searchKey});
            } else if (pairs.length === 0) {
                /* no pairs */
                res.status(404).send({error: 'pair not found', key: searchKey});
            } else {
                /* pair retrieved */
                let pair = pairs[0];
                res.status(200).send({message: 'value retrieved successfully!', value: pair.value});
            }
        });
    }
});

/* get all (key, value) pairs */
app.get('/getAllPairs', function (req, res) {
    KeyValue.find( function(error, pairs) {
        if (error) {
            res.status(500).send({error: 'error trying to fetch pairs: '+error});
        } else {
            /* pairs retrieved */
            res.status(200).send({message: 'pairs retrieved successfully!', pairs: pairs});
        }
    });
});

/* given a key, update it's value */
app.put('/updateValue', function (req, res) {
    let searchKey = req.body.key;
    let newValue = req.body.value;

    if (!searchKey) {
        /* bad request: no key sent */
        res.status(400).send({error: 'no key specified'});
    } else if (!newValue) {
        /* bad request: no value sent */
        res.status(400).send({error: 'no new value specified', key: searchKey});
    } else {
        KeyValue.findByKey(searchKey, function(error, pairs) {
            if (error) {
                res.status(500).send({error: 'error trying to update pair: '+error, key: searchKey});
            } else if (pairs.length === 0) {
                /* no pairs */
                res.status(404).send({error: 'pair not found', key: searchKey});
            } else {
                /* pair retrieved */
                let pair = pairs[0];
                pair.value = newValue;
                pair.save( function(error) {
                    if (error) {
                        res.status(500).send({error: 'error trying to update pair: '+error, key: searchKey});
                    } else {
                        res.status(200).send({message: 'pair updated successfully!', key: searchKey, newValue: newValue});
                    }
                });
            }
        });
    }
});

/* given a key, delete a pair */
app.delete('/deletePair', function (req, res) {
    let searchKey = req.body.key;
    if(!searchKey) {
        /* bad request: no key sent */
        res.status(400).send({error: 'no key specified'});
    } else {
        KeyValue.deleteOne({ key: searchKey }, function(error, result) {
            if (error) {
                res.status(500).send({error: 'error trying to delete pair: '+error, key: searchKey});
            } else {
                if (result.deletedCount === 0) {
                    res.status(404).send({error: 'pair not found', key: searchKey});
                } else {
                    res.status(200).send({message: 'pair deleted successfully!', key: searchKey});
                }
            }
        });
    }
});

var checkDB = function () {
    KeyValue.find( function(error, pairs) {
        if (error) {
            console.log("database error");
        } else {
            /* pairs retrieved */
            console.log("the database holds "+pairs.length+" documents");
        }
    });
};

app.listen(3000, function () {
    checkDB();
    console.log("hello MediaMonks! I'm listening");
});